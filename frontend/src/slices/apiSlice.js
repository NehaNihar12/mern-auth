import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';

const baseQuery = fetchBaseQuery({ baseUrl: '' }); // we are already using a proxy so we dont need this

export const apiSlice = createApi({
  baseQuery,
  tagTypes: ['User'], // this is for caching when we dont want to make fetch request everytime
  endpoints: (builder) => ({}),
});