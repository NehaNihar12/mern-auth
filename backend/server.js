import path from "path";
import express from "express";
import dotenv from "dotenv";
dotenv.config();
import cookieParser from "cookie-parser";
import { notFound, errorHandler } from "./middleware/errorMiddleware.js";
import connectDB from "./config/db.js";
const port = process.env.PORT || 5000;
import userRoutes from "./routes/userRoutes.js";

// connected to mongoDb database
connectDB();

const app = express();

// parse raw json
app.use(express.json());
// allow us to send form data
app.use(express.urlencoded({ extended: true }));

app.use(cookieParser());

app.use("/api/users", userRoutes);

if (process.env.NODE_ENV === "production") {
  const __dirname = path.resolve();
  // this will serve the static files from the frontend/build folder and
  // then '*' serve the index.html file for any other route that's not /api/users.
  app.use(express.static(path.join(__dirname, "frontend/build")));
  app.get("*", (req, res) =>
    res.sendFile(path.resolve(__dirname, "frontend", "build", "index.html"))
  );
} else {
  app.get("/", (req, res) => res.send("API is running...."));
}

app.get("/", (req, res) => res.send("Server is ready"));

app.use(notFound);
app.use(errorHandler);

app.listen(port, () => console.log(`Server started on port ${port}`));

// Routes we'll be creating

/**
 * - POST /api/users** - Register a user
 * - POST /api/users/auth** - Authenticate a user and get token
 * - POST /api/users/logout** - Logout user and clear cookie
 * - GET /api/users/profile** - Get user profile
 * - PUT /api/users/profile** - Update user profile
 */
