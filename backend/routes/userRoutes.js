import express from 'express';
const router = express.Router();
import { 
    authUser,
    registerUser,
    logoutUser,
    getUserProfile,
    updateUserProfile

} from '../controllers/userController.js';
import { protect } from '../middleware/authMiddleware.js';

router.post('/', registerUser);
router.post('/auth', authUser);
router.post('/logout', logoutUser);
// we can also write them in seperate lines or use route
router
.route('/profile')
.get(protect, getUserProfile)
.put(protect, updateUserProfile);


export default router;