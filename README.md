# mern-auth



## Getting started

- Make sure docker is installed on your system. It is a tool for managing containers.
 https://docs.docker.com/get-docker/

- Before building a Docker image, it is necessary to ensure that the Docker daemon is running. The Docker daemon is responsible for building and running Docker containers.
```
 on your terminal
 # start docker
 $sudo systemctl start docker
 # check status
 $sudo systemctl status docker
 #stop docker
 $sudo systemctl stop docker



```
- login to https://hub.docker.com/ which is a hosted repository service provided by Docker for finding and sharing container images with your team.

- link to my docker image: https://hub.docker.com/r/nehanihar/authspa

- copy the docker pull command from here
```
$docker pull nehanihar/authspa
```


## In your terminal

You need to be logged in to push or pull an image
```
$sudo docker login
username:
password:
```

```
#pull the image
$sudo docker pull nehanihar/authspa

#cmd to confirm you have that image
$sudo docker images

#run cmd to create your container
$sudo docker run --name <replace_with_your_container_name> -p 5000:${PORT:-5000} -p 3000:3000 -d nehanihar/authspa
```


## watch the changes on http://localhost:3000

The changes are deployed on port: 3000 of your machine.

## Starting and stopping containers

```
#list of all containers
$sudo docker ps -a

#list of running containers
$sudo docker ps

#list all images
$sudo docker images

#stop a container[you can also stop mutiple containers]
$sudo docker stop <your_container_name1>
or
$sudo docker stop <your_container_id1>

#start an existing container again
$sudo docker start <your_container_name1>
or
$sudo docker start <your_container_id1>

```

## Removing containers and Images permanently
```
#remove container/containers
$sudo docker rm <your_container_name1>
or
$sudo docker rm <your_container_id1>

#remove image/images
$sudo docker rmi <your_image_name1>
or
$sudo docker rmi <your_image_id1>

```

## Authors and acknowledgment
@author : Neha Nihar
<br>
gitlab link: https://gitlab.com/NehaNihar12/mern-auth

