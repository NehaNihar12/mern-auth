FROM node:17-alpine

WORKDIR /app

COPY package.json .

# Install backend dependencies
RUN npm install

# Change working directory to the frontend folder
WORKDIR /app/frontend

# Copy package.json and package-lock.json for frontend
COPY frontend/package.json .

# Install frontend dependencies
RUN npm install

# Change working directory back to the root of the project
WORKDIR /app

COPY . .

# Expose the ports for the server and client
EXPOSE ${PORT:-5000}
EXPOSE 3000
# required for docker desktop port mapping

CMD ["npm", "run", "dev"]